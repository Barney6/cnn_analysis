from container import Container
import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
name = "All_file"

storage = Container(root_name=name, thread_safe_mode=True)


key = storage.get_keys("images/nm0000001_rm577153792_1899-5-10_1968.jpg")

data = storage.get_original_image("nm0000001_rm577153792_1899-5-10_1968.jpg")

print(data)
