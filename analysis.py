import os
import caffe
from container import Container

class Analysis:
    def __init__(self, caffe_model, caffe_prototxt, images_folder, blobs_to_analyse=[], thread_safe=True, save_path=""):
        """
        Convolutional Neural Net Analysis tool.
        
        Parameters
        ----------
        :param caffe_model: str
            path to caffe model file
        :param caffe_prototxt: str
            path to prototxt file
        :param images_folder: str
            path to images folder to testing
        :param blobs_to_analyse: list
            list of layers to be analysed
        """
        caffe.set_mode_cpu()
        self.net = caffe.Net(caffe_prototxt, caffe_model, caffe.TEST)
        self.images_folder = images_folder
        self.predicted_age = 0
        self.thread_safe = thread_safe
        self.save_path = save_path
        if not thread_safe:
            self.container = Container("Full_analysis", thread_safe_mode=False, path=save_path)
        if not blobs_to_analyse:
            print('All Convolution and Fully connected layers to be analysed.')
            self.layers = [k for k, v in self.net.blobs.items()]
        else:
            print('Layers: ' + str(blobs_to_analyse) + ' will be analysed')
            self.layers = blobs_to_analyse

    def net_analyse(self, image_count):
        """
        Main run method to analyse the CNN.
        
        Parameters
        ----------
        :param image_count: int
            The amount of images in the folder to be analyse.
            Set to -1 for all images
        """
        if image_count == -1:
            image_count = len(os.listdir(self.images_folder))
            print('All ' + str(image_count) + ' images in folder will be processed.')

        if image_count > 0:
            print("First %d images in folder will be processed." % image_count)
            count = 0
            image_list = os.listdir(self.images_folder)

            for image in image_list:
                if "jpg" not in image:
                    continue
                im = self._net_run(image)
                if len(im) == 0:
                    continue
                layer_type = ""
                for layer in self.layers:
                    if layer != "data":
                        idx = list(self.net._layer_names).index(layer)
                        layer_type = self.net.layers[idx].type
                    else:
                        if self.thread_safe:
                            self.container = Container(root_name=image, path=self.save_path)
                        self.container.add_original_image(image_name=image, name="data", data=im)
                    if layer_type == 'Convolution':
                        self._conv_analyse(layer, image)
                    elif layer_type == 'InnerProduct':
                        self._fc_analyse(layer, image)
                    else:
                        print('Layer: ' + layer + ' ignored.')

                if count == image_count-1:
                    break
                else:
                    count += 1
                    images_remaining = image_count - count
                    print("%d images remaining." % images_remaining)
        else:
            raise ValueError("Image count must be -1 for all images, or > 0 for specific count. \
                            (image_count = %d)" % image_count)

    def _net_run(self, image):
        """
        Private method to run single image through the network.
        
        Parameters
        ----------
        :param image : jpg image
            The image to run though the net
        
        Returns
        -------
        :return : numpy.array
            The image returned from caffe.io.load_image
            
        """
        transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
        transformer.set_transpose('data', (2, 0, 1))
        transformer.set_channel_swap('data', (2, 1, 0))
        transformer.set_raw_scale('data', 255.0)
        try:
            im = caffe.io.load_image(self.images_folder + image)
        except (ValueError, RuntimeError, TypeError, NameError, ZeroDivisionError) as e:
            print("Image %s %s" % (image, str(e)))
            return []
        self.net.blobs['data'].data[...] = transformer.preprocess('data', im)
        out = self.net.forward()
        self.predicted_age = out['prob'].argmax()
        return im

    def _conv_analyse(self, layer, image):
        """
        Private method to analyse all neurons in convolution layer.
        
        Parameters
        ----------
        :param layer : str
            The convolution layer to be analysed 
        :param image : str
            The path to the image ran through the network
            
        """
        for neuron in range(len(self.net.blobs[layer].data[0])):
            conv_image = self.net.blobs[layer].data[0][neuron]
            self._save_data(layer=layer, neuron=neuron, data=conv_image, image_name=image)

    def _fc_analyse(self, layer, image):
        """
        Private method to analyse all neurons in fully connected layer.

        Parameters
        ----------
        :param layer : str
            The fully connected layer to be analysed 
        :param image : str
            The path to the image ran through the network
        """
        for neuron in range(len(self.net.blobs[layer].data[0])):
            activation_value = self.net.blobs[layer].data[0][neuron]
            self._save_data(layer=layer, neuron=neuron, data=activation_value, image_name=image)

    def _save_data(self, layer, neuron, data, image_name):
        """
        Private method to store the data in a container.
        
        Parameters
        ----------
        :param layer : str
            The name of the layer
        :param neuron : str
            The name of the neuron
        :param data : numpy.float or numpy.array
            The data to be saved
        :param image_name : str
            The name of the image
        """
        self.container.add_data(layer=layer, neuron=str(neuron), data=data, image_name=image_name)

