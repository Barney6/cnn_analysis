import os
import fnmatch
import h5py
import numpy as np


class Container:
    def __init__(self, root_name, path="", thread_safe_mode=True):
        """
        Creates the root container.

        Parameters
        ----------
        :param root_name : str
            Name of the root container - i.e. image name
        :param path : str
            Path of container storage (Defaults to current folder)
        """

        self.mode = thread_safe_mode
        if thread_safe_mode:
            print("Thread safe mode.")
            self.images = ""
        else:
            print("Non thread safe mode.")
            self.images = "images/"
        if os.path.isfile(path+root_name+".hdf5"):
            print("Opening: %s.hdf5" % (path+root_name))
            self.container = h5py.File(path + root_name + ".hdf5")
        else:
            print("Creating: %s.hdf5" % path+root_name)
            self.container = h5py.File(path + root_name + ".hdf5", "w")

    def add_original_image(self, image_name, name, data):
        """
        Add the original image.
        
        :param image_name : str
            The name of the base image
        :param name : str
            The name to store the image under
        :param data : numpy.array
            The image to be stores
        """
        self.__add_data_(path=self.images+"/"+image_name+"/"+name, data=data)

    def add_data(self, layer, neuron, data, image_name=""):
        """
        Adds data to container. 
        
        If the data is:
            numpy.array : stored in convolutions
            numpy.float       : stores in fully_connected
        
        Parameters
        ----------
        :param layer : str
            Layer of the dataset
        :param neuron : str
            Neuron of the dataset
        :param data : any
            Data to be stored
        :param image_name : str
            If used in non thread safe mode, image name is required
        """
        if not self.mode and image_name == "":
            raise TypeError("Image parameter must be passed in non thread safe mode.")
        if isinstance(data, np.ndarray):
            path = self.images+"%s/convolution/layer_%s/neuron_%s" % (image_name, layer, neuron)
            self.__add_data_(path, data)
        elif isinstance(data, np.float32):
            path = self.images+"%s/fully_connected/layer_%s/neuron_%s" % (image_name, layer, neuron)
            self.__add_data_(path, data)
        else:
            print("Data not added. %s" % type(data))

    def __add_data_(self, path, data):
        if path in self.container:
            self.container.__delitem__(path)
        self.container.create_dataset(path, data=data)

    def get_original_image(self, image_name, images="images", type="data"):
        return self.get_data_from_key("%s/%s/%s" % (images, image_name, type))

    def get_data(self, layer, neuron, image_name="", convolution=False, fc=False):
        """
        Retrieves data from container.

        Parameters
        ----------
        :param convolution : bool
            Set True to search convolution data
        :param fc : bool
            Set True to search fully_connected data
        :param layer : str
            Layer of the dataset
        :param neuron : str
            Neuron of the dataset
        :param image_name : str
            If used in non thread safe mode, image name is required

        Returns
        -------
        :return numpy.ndarray : for convolutions
            The convolved image
            
        :return float : for fully_connected
            The activation value
        """
        if convolution and len(image_name) > 0:
            path = self.images+"%s/convolution/layer_%s/neuron_%s" % (image_name, layer, neuron)
            return self.get_data_from_key(path)
        elif convolution:
            path = "convolution/layer_%s/neuron_%s" % (layer, neuron)
            return self.get_data_from_key(path)
        if fc and len(image_name) > 0:
            path = self.images+"%s/fully_connected/layer_%s/neuron_%s" % (image_name, layer, neuron)
            return self.get_data_from_key(path)
        elif fc:
            path = "fully_connected/layer_%s/neuron_%s" % (layer, neuron)
            return self.get_data_from_key(path)

    def get_data_from_key(self, path):
        if path in self.container:
            return self.container[path].value
        else:
            raise IOError("File does not exist: %s" % path)

    def get_keys(self, path):
        """
        Key structure
        
        Parameters
        ----------
        :param path : str
            The path of the group
        :return keys : list
            The keys in that group
        :return path : str
            The path name for that group
        """
        keys = self.container[path].keys()
        path = self.container[path].name
        return keys, path

    def print_all_group_names(self):
        """
        Prints all groups and dataset names.
        
        """
        self.container.visit(func=self.__print_name_)

    @staticmethod
    def merge_files(path_to_files):
        """
        Merges folder of hdf5 files together
        
        :param path_to_files : str
            Path of folder with hdf5 files
        """
        new_file = h5py.File("All_file.hdf5")
        new_file.create_group("images")
        files = os.listdir(path_to_files)
        for h5_file in files:
            if not h5_file.endswith(".hdf5"):
                continue
            group = os.path.splitext(h5_file)[0]
            open_file = h5py.File(path_to_files+"/"+h5_file, "r")
            open_file.flush()
            h5py.h5o.copy(open_file.id, group, new_file.id, "images/"+group)
            print("finished")

    @staticmethod
    def delete_container_file(path_to_file):
        if fnmatch.fnmatch(path_to_file, '*.hdf5'):
            print("Deleting %s" % path_to_file)
            os.remove(path_to_file)
        else:
            raise IOError("File must be .hdf5 format: %s" % path_to_file)

    @staticmethod
    def __print_name_(name):
        print(name)

    def __del__(self):
        self.container.close()
