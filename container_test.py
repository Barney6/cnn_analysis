from container import Container
import matplotlib.pyplot as plt
import cv2

storage = Container("im/nm0000001_rm577153792_1899-5-10_1968.jpg", thread_safe_mode=True)


# storage.print_all_group_names()

data = storage.get_data(layer="conv3_2", neuron="0", image_name="nm0000001_rm577153792_1899-5-10_1968.jpg", convolution=True)

# storage.delete_container_file("root.hdf5")

plt.imshow(data, cmap='Greys')
plt.show()
