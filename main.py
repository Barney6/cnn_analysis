from analysis import Analysis
import sys

caffe_model = sys.argv[1]
caffe_prototxt = sys.argv[2]
image_folder = sys.argv[3]
image_count = sys.argv[4]
save_path = sys.argv[5]

analysis = Analysis(caffe_model, caffe_prototxt, image_folder, thread_safe=True, save_path=save_path)

analysis.net_analyse(int(image_count))
